package com.org.ecatalog.mapper;

import com.org.ecatalog.dto.SchoolClassDto;
import com.org.ecatalog.dto.StudentCreateDto;
import com.org.ecatalog.dto.StudentDto;
import com.org.ecatalog.entity.Student;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class StudentMapper {

    private final SchoolClassMapper schoolClassMapper;

    public Student toEntity(StudentCreateDto createDto) {
        return Student.builder()
                .firstName(createDto.getFirstName())
                .lastName(createDto.getLastName())
                .cnp(createDto.getCnp())
                .schoolClassId(createDto.getSchoolClassId())
                .build();

    }


    // (StudentDto <- SchoolClassDto)    <=  school class mapper  <=   (Student <- SchoolClass)
    public StudentDto toDto(Student entity) {

        SchoolClassDto schoolClassDto = schoolClassMapper.toDto(entity.getSchoolClass());

        return StudentDto.builder()
                .id(entity.getId())
                .cnp(entity.getCnp())
                .firstName(entity.getFirstName())
                .lastName(entity.getLastName())
                .schoolClass(schoolClassDto)
                .build();

    }

    public List<StudentDto> toDtoList(List<Student> studentList) {
        List<StudentDto> studentDtoList = new ArrayList<>();
        for (Student student : studentList) {
            StudentDto studentDto = toDto(student);
            studentDtoList.add(studentDto);
        }
        return studentDtoList;
    }
}
