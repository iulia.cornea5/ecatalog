package com.org.ecatalog.controller;

import com.org.ecatalog.dto.StudentCreateDto;
import com.org.ecatalog.dto.StudentDto;

import com.org.ecatalog.service.StudentService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("students")
@AllArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public ResponseEntity<StudentDto> create(@RequestBody StudentCreateDto studentCreateDto) {
        return ResponseEntity.ok(studentService.create(studentCreateDto));
    }

    @GetMapping
    public List<StudentDto> getAll() {
        return studentService.getAll();
    }

    // exemplu: http://localhost:8080/school-class/d82f2e1a-5915-4d81-85dd-c86c73c47435
    @GetMapping("/school-class/{id}")
    public List<StudentDto> getAll(@PathVariable(name = "id") UUID id) {
        return studentService.getAllFromClass(id);
    }


}
