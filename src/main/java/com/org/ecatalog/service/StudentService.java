package com.org.ecatalog.service;

import com.org.ecatalog.dto.StudentCreateDto;
import com.org.ecatalog.dto.StudentDto;
import com.org.ecatalog.entity.Student;
import com.org.ecatalog.mapper.StudentMapper;
import com.org.ecatalog.repository.StudentRepository;
import jakarta.persistence.EntityManager;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final StudentMapper studentMapper;
    private final EntityManager entityManager;

    public StudentDto create(StudentCreateDto createDto) {
        Student toBeSaved = studentMapper.toEntity(createDto);
        Student saved = studentRepository.save(toBeSaved);
        entityManager.clear();
        Student studentWithClass = studentRepository.findById(saved.getId()).get();
        return studentMapper.toDto(studentWithClass);
    }

    public List<StudentDto> getAll() {
        List<Student> students = studentRepository.findAll();
        return studentMapper.toDtoList(students);
    }

    public List<StudentDto> getAllFromClass(UUID schoolClassId) {
        List<Student> studentList = studentRepository.findAllBySchoolClassId(schoolClassId);
       return studentMapper.toDtoList(studentList);
    }

}
