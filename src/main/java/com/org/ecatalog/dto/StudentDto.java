package com.org.ecatalog.dto;

import lombok.*;

import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class StudentDto {

    private UUID id;

    private String cnp;

    private String firstName;

    private String lastName;

    private SchoolClassDto schoolClass;
}
